<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::factory(15)->create();

        Product::factory(200)
            ->create()
            ->each(function ($product) use ($categories) {
                $product->categories()
                    ->attach(
                        $categories->random(
                            rand(0, $categories->count() / 3)
                        )
                    );
            });
    }
}
