<?php

namespace Database\Factories;

use App\Enums\ProductStatus;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'sku' => fake()->unique()->regexify('[A-Z]{3}[A-Z0-9]{10}'),
            'name' => fake()->words(3, true),
            'description' => fake()->text(),
            'price' => fake()->randomFloat(2, max: 10000),
            'stock' => fake()->randomNumber(4),
        ];
    }
}
