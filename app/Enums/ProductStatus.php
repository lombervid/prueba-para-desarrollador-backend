<?php

namespace App\Enums;

enum ProductStatus: string
{
    case IN_STOCK = 'In stock';
    case OUT_OF_STOCK = 'Out of stock';
}
