<?php

namespace App\Models;

use App\Enums\ProductStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['sku', 'name', 'description', 'price', 'stock'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'status' => ProductStatus::class,
    ];

    /**
     * The roles that belong to the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    protected static function booted()
    {
        static::saving(function ($product) {
            // Update `status` when `stock` is modified
            if ($product->isDirty('stock')) {
                $product->status = $product->stock ? ProductStatus::IN_STOCK : ProductStatus::OUT_OF_STOCK;
            }
        });
    }
}
