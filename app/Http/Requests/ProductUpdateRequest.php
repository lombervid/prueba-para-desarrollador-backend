<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'sku' => 'string|unique:products',
            'name' => 'string',
            'description' => 'string',
            'price' => 'numeric|min:0',
            'stock' => 'int|numeric|min:0',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (empty($this->validated())) {
                $validator->errors()->add('empty', 'At least one field must be provided.');
            }
        });
    }
}
